package com.info.dryright.Activity.API.Retrofit;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    /************ Production Base Url *************/
    public static final String BASE_URL="";
    /**************** Live Base URl ****************/
    public static OkHttpClient okHttpClient= null;
    public static Retrofit retrofit = null;
    public static String token="";
    public static Retrofit getClient(){
        if (retrofit == null){
            retrofit= new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(getHeaderClient())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
    public static OkHttpClient getRequestHeader(){
        if (null == okHttpClient){
            okHttpClient= new OkHttpClient.Builder()
                    .readTimeout(60, TimeUnit.SECONDS)
                    .connectTimeout(60,TimeUnit.SECONDS)
                    .build();
        }
        return okHttpClient;
    }
    public static OkHttpClient getHeaderClient(){
        OkHttpClient.Builder httpClient= new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request orignal= chain.request();
                Request request= orignal.newBuilder()
                        .header("Authorization","Bearer "+token)
                        .header("Content-Type","application/json")
                        .method(orignal.method(), orignal.body())
                        .build();
                return chain.proceed(request);
            }
        });
        OkHttpClient client= httpClient.build();
        return client;
    }
}
