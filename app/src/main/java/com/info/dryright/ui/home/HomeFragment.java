package com.info.dryright.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.info.dryright.Activity.API.models.PeopleList;
import com.info.dryright.Activity.Adapter.Ad_peopleList;
import com.info.dryright.R;
import com.info.dryright.databinding.FragmentHomeBinding;
import com.info.dryright.ui.CustomerInfo.CustomerInfoFragment;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private FragmentHomeBinding binding;
    List<PeopleList> peopleLists;
    RecyclerView rv_home_list;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);

        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

       // final TextView textView = binding.textHome;
        rv_home_list = binding.rvHomeList;
        homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
               // textView.setText(s);
            }
        });
        peopleLists= new ArrayList<>();
        getPeopleList();
        return root;
    }
    public void getPeopleList(){
        PeopleList peopleList= new PeopleList();
        peopleList.setmId(1);
        peopleList.setmName("Herry pitor");
        peopleList.setAddress("12A-10, Derlington,UK");
        peopleList.setPhone("12345678910");
        peopleList.setOpenClose("open");
        peopleLists.add(peopleList);

        PeopleList peopleList1= new PeopleList();
        peopleList1.setmId(2);
        peopleList1.setmName("Teena Lisa");
        peopleList1.setAddress("12A-10, Derlington,UK");
        peopleList1.setPhone("12345678910");
        peopleList1.setOpenClose("close");
        peopleLists.add(peopleList1);

        PeopleList peopleList2= new PeopleList();
        peopleList2.setmId(3);
        peopleList2.setmName("Same san");
        peopleList2.setAddress("12A-10, Derlington,UK");
        peopleList2.setPhone("12345678910");
        peopleList2.setOpenClose("open");
        peopleLists.add(peopleList2);

        PeopleList peopleList4= new PeopleList();
        peopleList4.setmId(4);
        peopleList4.setmName("Simona deyl");
        peopleList4.setAddress("12A-10, Derlington,UK");
        peopleList4.setPhone("12345678910");
        peopleList4.setOpenClose("close");
        peopleLists.add(peopleList4);

        PeopleList peopleList5= new PeopleList();
        peopleList5.setmId(5);
        peopleList5.setmName("Ritesh Bagul");
        peopleList5.setAddress("12A-10, Derlington,UK");
        peopleList5.setPhone("12345678910");
        peopleList5.setOpenClose("open");
        peopleLists.add(peopleList5);

        PeopleList peopleList6= new PeopleList();
        peopleList6.setmId(6);
        peopleList6.setmName("Pooja Nenava");
        peopleList6.setAddress("12A-10, Derlington,UK");
        peopleList6.setPhone("12345678910");
        peopleList6.setOpenClose("open");
        peopleLists.add(peopleList6);

        PeopleList peopleList7= new PeopleList();
        peopleList7.setmId(7);
        peopleList7.setmName("Shweta Goyal");
        peopleList7.setAddress("12A-10, Derlington,UK");
        peopleList7.setPhone("12345678910");
        peopleList7.setOpenClose("open");
        peopleLists.add(peopleList7);

        PeopleList peopleList8= new PeopleList();
        peopleList8.setmId(8);
        peopleList8.setmName("Panjak Evane");
        peopleList8.setAddress("12A-10, Derlington,UK");
        peopleList8.setPhone("12345678910");
        peopleList8.setOpenClose("open");
        peopleLists.add(peopleList8);
        LinearLayoutManager layoutManager=new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        rv_home_list.setLayoutManager(layoutManager);
        rv_home_list.setItemAnimator(new DefaultItemAnimator());
        Ad_peopleList adapterPeople=new Ad_peopleList(peopleLists,getContext(),new Ad_peopleList.ListClickListener(){
            @Override
            public void peopleClick(PeopleList list) {
                CustomerInfoFragment infoFragment=new CustomerInfoFragment();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.nav_host_fragment_activity_main,infoFragment,"next Fragment")
                        .addToBackStack(null)
                        .commit();
            }
        });
        rv_home_list.setAdapter(adapterPeople);

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}