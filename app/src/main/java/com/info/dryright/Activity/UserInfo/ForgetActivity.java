package com.info.dryright.Activity.UserInfo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import com.info.dryright.Utility.ErrorLayout;
import com.info.dryright.Utility.Utility;
import com.info.dryright.databinding.ActivityForgetBinding;

public class ForgetActivity extends AppCompatActivity {

    ActivityForgetBinding binding;
    String emailId="";
    Utility utility;
    ErrorLayout errorLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //setContentView(R.layout.activity_forget);
        binding= ActivityForgetBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        utility= new Utility();
        emailId=binding.forgetEtEmailId.getText().toString().trim();
        errorLayout= new ErrorLayout(binding.errorView.errorRl);
        //errorLayout=binding.errorView.errorRl;

        binding.forgetSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                errorLayout.hideError();
                if (emailId.isEmpty()){
                    utility.showToast(ForgetActivity.this,"please enter email Id.");
                    errorLayout.showError("please enter email");
                    return;

                }else if (!Utility.isValidEmail(emailId)){
                    utility.showToast(ForgetActivity.this,"please enter valid Email.");
                    errorLayout.showError("please enter valid Email");
                    return;
                }else {
                    api_forgotPassword();
                }
            }
        });

    }
    public void api_forgotPassword(){

    }
}