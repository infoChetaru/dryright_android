package com.info.dryright.Utility;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.os.Vibrator;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;


import com.info.dryright.R;

import java.util.concurrent.atomic.AtomicBoolean;

public class ErrorLayout {
    public static final long LENGTH_SHOT= 1500L;
    public static  final long LENGTH_LONG= 5500L;

    private final Handler handler = new Handler();
    public TextView mErrorTv;
    Vibrator vVibrator;
    private Animation mMoveUpAnim, mMoveDownAnim;
    private AtomicBoolean mErrorShow;
    private ErrorLayoutListener mErrorLayoutListener;

    public ErrorLayout(View view){
        try {
            mErrorTv = view.findViewById(R.id.error_tv);
            vVibrator = (Vibrator) view.getContext().getSystemService(Context.VIBRATOR_SERVICE);
            mErrorTv.setVisibility(View.GONE);
            mMoveUpAnim = AnimationUtils.loadAnimation(view.getContext(),R.anim.move_up);
            mMoveDownAnim = AnimationUtils.loadAnimation(view.getContext(),R.anim.move_down);
            mErrorShow = new AtomicBoolean(false);

        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void showAlert(String error,MsgType msgType){
        mErrorTv.setVisibility(View.VISIBLE);
        mErrorTv.setText(error);
        int color=0;
        switch (msgType){
            case Error:
                color= Color.RED;
                break;
            case Success:
                color=Color.GREEN;
                break;
            case Info:
                color= Color.DKGRAY;
                break;
            case Warning:
                color=Color.YELLOW;
                break;
        }
        mErrorTv.setBackgroundColor(color);
        if (!mErrorShow.get()){
            mErrorShow.set(true);
            vVibrator.vibrate(150);
            mErrorTv.startAnimation(mMoveDownAnim);
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mErrorTv.startAnimation(mMoveDownAnim);
                    mErrorShow.set(false);
                    if (null != mErrorLayoutListener){
                        mErrorLayoutListener.onErrorHidden();
                    }
                }
            }, 100 <= error.length() ? LENGTH_LONG : LENGTH_SHOT);
        }
    }

    public void showError(String error){
        mErrorTv.setVisibility(View.VISIBLE);
        mErrorTv.setText(error);
        mErrorTv.setBackgroundColor(R.color.dark_blue);
        if (!mErrorShow.get()){
            mErrorShow.set(true);
            vVibrator.vibrate(150);
            mErrorTv.startAnimation(mMoveDownAnim);

        }
    }
    public void hideError(){
        try {
            if (mErrorTv.getVisibility() == View.VISIBLE) {
                mErrorTv.startAnimation(mMoveUpAnim);
                mErrorShow.set(false);
                if (null != mErrorLayoutListener) {
                    mErrorLayoutListener.onErrorHidden();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void setmErrorLayoutListener(ErrorLayoutListener listener) {
        this.mErrorLayoutListener = listener;
    }

    public enum MsgType{
        Error, Success,Info,Warning
    }
    public interface ErrorLayoutListener{
        void onErrorShown();
        void onErrorHidden();
    }
}
