package com.info.dryright.Activity.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.info.dryright.Activity.API.models.PeopleList;
import com.info.dryright.R;

import java.util.List;

public class Ad_peopleList extends RecyclerView.Adapter<Ad_peopleList.ViewHolder> {
    List<PeopleList> list;
    Context context;
    ListClickListener mListener;

    public Ad_peopleList(List<PeopleList> list, Context context ,ListClickListener mListener) {
        this.list = list;
        this.context = context;
        this.mListener=mListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.row_people_list,parent,false);
        return new ViewHolder(view);
    }



    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try{
            holder.tv_title_name.setText(list.get(position).getmName());
            holder.address_tv.setText(list.get(position).getAddress());
            holder.phone_tv.setText(list.get(position).getPhone());
            holder.open_tv.setText(list.get(position).getOpenClose());
            if (list.get(position).getOpenClose().equals("open")){
                holder.close_image.setImageResource(R.drawable.status);
            }else{
                holder.close_image.setImageResource(R.drawable.status_close);
            }
            holder.main_ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.peopleClick(list.get(position));
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        try{
            return list.size();
        }catch (Exception e) {
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView tv_title_name;
        TextView address_tv,phone_tv,open_tv;
        ImageView close_image;
        LinearLayout main_ll;
        public ViewHolder(View v){
            super(v);
            tv_title_name= v.findViewById(R.id.title_name_tv);
            address_tv=v.findViewById(R.id.address_tv);
            phone_tv=v.findViewById(R.id.phone_tv);
            close_image=v.findViewById(R.id.close_image);
            open_tv=v.findViewById(R.id.open_tv);
            main_ll=v.findViewById(R.id.main_ll);

        }
    }

    public interface ListClickListener{
        public void peopleClick(PeopleList list);
    }
}
