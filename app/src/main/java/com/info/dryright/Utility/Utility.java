package com.info.dryright.Utility;

import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.provider.Settings;
import android.widget.Toast;

import androidx.core.graphics.drawable.DrawableCompat;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;


public class Utility {
    public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
    );
    String result="";
    StringBuilder sb;
    String startdate="";
    public static boolean isAppRunning(final Context context,final String packageName){
        final ActivityManager activityManager= (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningAppProcessInfo> procInfos=activityManager.getRunningAppProcesses();
        if (procInfos != null){
            for (final ActivityManager.RunningAppProcessInfo processInfo : procInfos){
                if (processInfo.processName.equals(packageName)){
                    return true;
                }
            }
        }
        return false;
    }
    public static Drawable tintMyDrawable(Drawable drawable,int color){
        drawable= DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable,color);
        DrawableCompat.setTintMode(drawable, PorterDuff.Mode.SRC_IN);
        return drawable;
    }

    public static String format(Number n){
        NumberFormat format= DecimalFormat.getInstance();
        format.setRoundingMode(RoundingMode.FLOOR);
        format.setMinimumFractionDigits(0);
        format.setMinimumFractionDigits(2);
        return format.format(n);
    }

    public static Bitmap decodeSampledBitmapFromResource(String path,int reqWidth, int reqHeight){
        //First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options=new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path,options);

        //Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options,reqWidth, reqHeight);
        //Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path,options);

    }
    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and
            // width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will
            // guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        return inSampleSize;
    }

    public static boolean isValidEmail(CharSequence target){
        if (target == null|| target.toString().isEmpty()){
            return  false;
        }else {
            return EMAIL_ADDRESS_PATTERN.matcher(target.toString()).matches();
        }
    }
    public <T> Set<T> findDuplicates(Collection<T> collection){
        Set<T> duplicates= new LinkedHashSet<>();
        Set<T> uniques= new HashSet<>();
        for (T t: collection){
            if (!uniques.add(t)){
                duplicates.add(t);
            }
        }
        return  duplicates;
    }
    public void showToast(Context context,String message){
        Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
    }
    public String getAndroidID(Context context){
        String android_id = Settings.System.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        return android_id;

    }
}
