package com.info.dryright.Session;

import android.content.Context;
import android.content.SharedPreferences;

public class SessionParam {
    Context context;
    String PREF_NAME="MyPref";
    String FCM_NAME="FCMPref";
    public String deviceId="";
    String fcm_token="";


    public void saveDeviceId(Context context,String deviceId){
        SharedPreferences sharedPreferences= context.getSharedPreferences(
                PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor= sharedPreferences.edit();
        prefsEditor.putString("deviceId",deviceId);
        prefsEditor.commit();
    }
    public void saveFCM_token(Context context,String fcm_token){
        SharedPreferences sharedPreferences=context.getSharedPreferences(
                    FCM_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor=sharedPreferences.edit();
        prefsEditor.putString("fcm_token",fcm_token);
        prefsEditor.commit();

    }
    public void clearPrefrence(Context context){
        SharedPreferences sharedPreferences= context.getSharedPreferences(
                PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor= sharedPreferences.edit();
        prefsEditor.clear();
        prefsEditor.commit();
    }

    @Override
    public String toString() {
        return "SessionParam{" +
                "context=" + context +
                ", PREF_NAME='" + PREF_NAME + '\'' +
                ", FCM_NAME='" + FCM_NAME + '\'' +
                ", deviceId='" + deviceId + '\'' +
                ", fcm_token='" + fcm_token + '\'' +
                '}';
    }
}
