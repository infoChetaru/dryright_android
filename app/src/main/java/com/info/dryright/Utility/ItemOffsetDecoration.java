package com.info.dryright.Utility;

import android.content.Context;
import android.graphics.Rect;
import android.view.View;

import androidx.annotation.DimenRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

public class ItemOffsetDecoration extends RecyclerView.ItemDecoration {
    private int mItemOffSet;
    public ItemOffsetDecoration(int itemOffset){
        mItemOffSet= itemOffset;
    }
    public ItemOffsetDecoration(@NotNull Context context, @DimenRes int itemOFFSetId){
        this(context.getResources().getDimensionPixelSize(itemOFFSetId));
    }

    @Override
    public void getItemOffsets(@NonNull @NotNull Rect outRect, @NonNull @NotNull View view, @NonNull @NotNull RecyclerView parent, @NonNull @NotNull RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        outRect.set(mItemOffSet,mItemOffSet,mItemOffSet,mItemOffSet);
    }
}
