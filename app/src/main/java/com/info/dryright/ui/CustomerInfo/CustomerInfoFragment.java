package com.info.dryright.ui.CustomerInfo;

import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.info.dryright.R;
import com.info.dryright.databinding.CustomerInfoFragmentBinding;

public class CustomerInfoFragment extends Fragment {

    private CustomerInfoViewModel mViewModel;
    private CustomerInfoFragmentBinding binding;

    public static CustomerInfoFragment newInstance() {
        return new CustomerInfoFragment();
    }

    TextView custom_title_name_tv,custom_address_tv,mail_open_tv,custom_phone_tv;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.customer_info_fragment, container, false);

        binding = CustomerInfoFragmentBinding.inflate(inflater,container,false);
        View root=binding.getRoot();
        custom_title_name_tv= view.findViewById(R.id.custom_title_name_tv);
        custom_address_tv=view.findViewById(R.id.custom_address_tv);
        mail_open_tv=view.findViewById(R.id.mail_open_tv);
        custom_phone_tv=view.findViewById(R.id.custom_phone_tv);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(CustomerInfoViewModel.class);
       // TODO: Use the ViewModel
    }

}