package com.info.dryright.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.WindowManager;
import android.view.inputmethod.InputMethod;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.info.dryright.Activity.UserInfo.ForgetActivity;
import com.info.dryright.MainActivity;
import com.info.dryright.R;
import com.info.dryright.Utility.ErrorLayout;
import com.info.dryright.Utility.Utility;
import com.info.dryright.databinding.ActivityLoginBinding;

import java.util.List;

public class LoginActivity extends AppCompatActivity {

    EditText et_email_id;
    EditText et_password;
    TextView sing_in_button;
    TextView tv_forgotPass;
    RelativeLayout main_ll;
    Utility utility;
    private ErrorLayout errorLayout;
    //private ActivityLoginBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);

        errorLayout= new ErrorLayout(findViewById(R.id.error_rl));
        utility= new Utility();
        et_email_id=findViewById(R.id.et_email_id);
        et_password=findViewById(R.id.et_password);
        sing_in_button=findViewById(R.id.sign_in_button);
        tv_forgotPass=findViewById(R.id.tv_forgotPass);
        main_ll=findViewById(R.id.main_ll);
        main_ll.setOnClickListener(v->{
            InputMethodManager inputMethodManager= (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(),0);
        });
        sing_in_button.setOnClickListener(v->{
            try {
                errorLayout.hideError();
                getLoginAPi();
                String emailId=et_email_id.getText().toString().trim();
                if (validation()){
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        });


        tv_forgotPass.setOnClickListener(v->{
            try {
                Intent intent= new Intent( LoginActivity.this, ForgetActivity.class);
                startActivity(intent);
            }catch (Exception e){
                e.printStackTrace();
            }
        });
    }
    public boolean validation(){
        if (errorLayout==null){
            errorLayout= new ErrorLayout(findViewById(R.id.error_rl));
        }
        String emailId=et_email_id.getText().toString().trim();
        String password=et_password.getText().toString().trim();
        if (emailId.isEmpty()){
            errorLayout.showError("please enter email");
                return false;
        }else if (Utility.isValidEmail(emailId)){
            errorLayout.showError("please enter valid email");
            return false;
        }else if (password.isEmpty()){
            errorLayout.showError("please enter password");
            return false;
        }else {
            return true;
        }
    }
    public void getLoginAPi(){
        Intent intent= new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}