package com.info.dryright.Utility;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkConnection {
    public static boolean checkNetrokStatus(Context context){
        boolean HaveConnectedWifi = false;
        boolean HaveConnectedMibile = false;

        try{
            ConnectivityManager cm= (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo[] netInfo= cm.getAllNetworkInfo();
            for (NetworkInfo ni : netInfo){
                if (ni.getTypeName().equalsIgnoreCase("WIFI")){
                    if (ni.isConnected())
                        HaveConnectedWifi = true;
                }
                if (ni.getTypeName().equalsIgnoreCase("MOBILE")){
                    if (ni.isConnected())
                        HaveConnectedMibile=true;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return HaveConnectedWifi || HaveConnectedMibile;
    }

}

